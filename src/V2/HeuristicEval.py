def heuristicEval(agent, neighbour):
    """
    Heuristic evaluation function
    """
    neighbour_distance = neighbour.get('d')
    neighbour_ammo = neighbour.get('ammo')
    neighbour_life = neighbour.get('life')
    current_life = agent.vie
    current_ammo = agent.munitions
    # Make a formula to calculate the cost of the neighbour
    cost = abs(neighbour_distance * 0.1) - ((abs(current_ammo - neighbour_ammo)) * 0.2) + (abs((current_life - neighbour_life)) * 0.3)
    return cost