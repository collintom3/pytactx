from src.V2.BaseState import BaseState
from src.V2.EnumState import EnumState


class KillState(BaseState):
    def __init__(self, agent):
        """
        Constructor of KillState
        """
        super().__init__(agent, (255, 0, 0))

    def onHandle(self):
        """
        Handle the state
        """
        target = self._agent.getTarget()
        if target is not None:
            # if agent is not close to the target, agent move to the target
            if self._agent.x != target.get('x') and self._agent.y != target.get('y'):
                self._agent.deplacer(target.get('x'), target.get('y'))
            else:
                # if agent is close to the target, agent shoot
                self._agent.tirer(True)
                self._agent.tirer(False)
            # if target is dead, agent switch to decision state
                if target.get('life') == 0:
                    self._agent.setTarget(None)
                    self.switchState(EnumState.DECISION.value)
