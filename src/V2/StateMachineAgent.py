from src import pytactx
from src.V2.DecisionState import DecisionState
from src.V2.EnumState import EnumState
from src.V2.KillState import KillState
from src.V2.StateMachine import StateMachine
from src import pytactx


class StateMachineAgent(pytactx.Agent):
    def __init__(self, myId):
        """
        Constructor of Agent
        """
        self._target = None
        # create state machine
        self.__stateMachine = None
        self.__state_machine = StateMachine()
        self.__state_machine.addState(DecisionState(self))
        self.__state_machine.addState(KillState(self))
        # TODO _pwd = input('pass ? ')
        super().__init__(id=myId,
                         username="demo",
                         password="demo",
                         arena="demo",
                         server="mqtt.jusdeliens.com",
                         prompt=False,
                         verbose=False)

        self.__state_machine.setState(EnumState.DECISION.value)

    def getTarget(self):
        """
        Get the target of the agent
        """
        return self._target

    def setTarget(self, target):
        """
        Set the target of the agent
        """
        self._target = target

    def quandActualiser(self):
        """
        Called when the agent is updated
        """
        self.__state_machine.handle()
