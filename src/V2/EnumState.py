from enum import Enum


class EnumState(Enum):
    """EnumState class"""
    DECISION = 0
    KILL = 1
