from src.V2.State import State


class StateMachine:
    def __init__(self):
        """
        Constructor of StateMachine
        """
        self.__state = 0
        self.__states = []

    def addState(self, state: State):
        """
        Add a state to the state machine
        """
        state.setContext(self)
        self.__states.append(state)

    def setState(self, state: int):
        """
        Set the state of the state machine
        """
        self.__state = state

    def handle(self):
        """
        Handle the state machine
        """
        self.__states[self.__state].handle()
