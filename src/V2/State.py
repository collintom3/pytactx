from abc import ABC

from src.V2.IState import IState


class State(IState, ABC):
    def __init__(self):
        """
        Constructor of State
        """
        self._context = None

    def setContext(self, context):
        """
        Set the context of the state
        """
        self._context = context

    def switchState(self, state):
        """
        Switch the state of the state machine
        """
        self._context.setState(state)
