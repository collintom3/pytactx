from abc import abstractmethod

from src.V2.State import State


class BaseState(State):
    def __init__(self, agent, color):
        """
        Constructor of BaseState
        """
        self._color = color
        self._agent = agent
        super().__init__()

    def handle(self):
        r, g, b = self._color
        self._agent.changerCouleur(r, g, b)
        self.onHandle()

    @abstractmethod
    def onHandle(self):
        ...
