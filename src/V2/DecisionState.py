from src.V2.BaseState import BaseState
from src.V2.EnumState import EnumState
from src.V2.HeuristicEval import heuristicEval


class DecisionState(BaseState):
    def __init__(self, _agent):
        """
        Constructor of DecisionState
        """
        super().__init__(_agent, (0, 255, 0))

    def onHandle(self):
        """
        Handle the state
        """
        print("DecisionState")
        #Get all neighbours of the _agent
        neighbours = self._agent.voisins
        if len(neighbours) != 0:
            # Get the best neighbour
            best_neighbour = self.getBestNeighbour(neighbours)
            # Save the best neighbour in _agent's memory
            self._agent.setTarget(best_neighbour)
            print("Best neighbour: ", best_neighbour)
            # Switch to kill state
            self.switchState(EnumState.KILL.value)

    def getBestNeighbour(self, neighbours):
        """
        Get the best neighbour
        """
        best_neighbour = None
        best_score = -1
        for neighbour, value in neighbours.items():
            heuristic_score = heuristicEval(self._agent, value)
            if heuristic_score > best_score:
                best_score = heuristic_score
                best_neighbour = value
        return best_neighbour
