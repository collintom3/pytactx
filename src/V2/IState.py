from abc import abstractmethod, ABC


class IState(ABC):
    @abstractmethod
    def handle(self):
        pass